package bssolrus.com.quiz;

import android.content.Context;

import bssolrus.com.quiz.util.PrefsUtils;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/10/15.
 */
public class Settings {
    private static final String LAST_QUIZ_CATEGORY_ID = "quiz categoryId";
    private static final String LAST_QUIZ_MARKET_ID = "quiz marketId";
    private static final String LAST_SELECTED_MARKET_ID = "selected marketId";

    public static String getLastQuizCategoryId(Context context) {
        return PrefsUtils.getString(context, LAST_QUIZ_CATEGORY_ID, null);
    }

    public static String getLastQuizMarketId(Context context) {
        return PrefsUtils.getString(context, LAST_QUIZ_MARKET_ID, null);
    }

    public static void clearLastQuizCategoryId(Context context) {
        PrefsUtils.remove(context, LAST_QUIZ_CATEGORY_ID);
    }

    public static void clearLastQuizMarketId(Context context) {
        PrefsUtils.remove(context, LAST_QUIZ_MARKET_ID);
    }

    public static void saveLastQuizCategoryId(Context context, String categoryId) {
        PrefsUtils.putString(context, LAST_QUIZ_CATEGORY_ID, categoryId);
    }

    public static void saveLastQuizMarketId(Context context, String marketId) {
        PrefsUtils.putString(context, LAST_QUIZ_MARKET_ID, marketId);
    }

    public static void saveLastSelectedMarketId(Context context, String marketId) {
        PrefsUtils.putString(context, LAST_SELECTED_MARKET_ID, marketId);
    }

    public static String getLastSelectedMarketId(Context context) {
        return PrefsUtils.getString(context, LAST_SELECTED_MARKET_ID, null);
    }
}
