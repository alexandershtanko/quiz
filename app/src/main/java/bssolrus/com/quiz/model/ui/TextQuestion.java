package bssolrus.com.quiz.model.ui;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class TextQuestion extends Question<String> {
    protected TextQuestion(String questionId, String text) {
        this.setQuestionId(questionId);
        this.setText(text);
        this.setQuestionType(QuestionTypes.QUESTION_TYPE_TEXT);
    }

    @Override
    public boolean isAnswerSet() {
        return answer != null && answer.length() > 0;
    }
}
