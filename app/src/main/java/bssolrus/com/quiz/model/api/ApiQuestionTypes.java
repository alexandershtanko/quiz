package bssolrus.com.quiz.model.api;

/**
 * Created by aleksandr on 08.11.15.
 */
public class ApiQuestionTypes {
    public static final String YES_NO_QUESTION = "yes_no";
    public static final String VARIANTS_QUESTION = "variants";
    public static final String TEXT_QUESTION = "text";
    public static final String PHOTO_QUESTION = "photo";
}
