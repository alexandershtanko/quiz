package bssolrus.com.quiz.model.ui;

import com.orm.SugarRecord;

/**
 * Created by aleksandr on 04.11.15.
 */
public abstract class Question<AnswerType> extends SugarRecord<Question> {
    AnswerType answer;
    private Boolean isChanged;
    private int questionType;
    private String text;
    private String questionId;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getQuestionType() {
        return questionType;
    }

    protected void setQuestionType(int questionType) {
        this.questionType = questionType;
    }

    public AnswerType getAnswer() {
        return answer;
    }

    public abstract boolean isAnswerSet();

    public void setAnswer(AnswerType answer) {
        this.answer = answer;
    }


    public Boolean getIsChanged() {
        return isChanged;
    }

    public void setIsChanged(Boolean isChanged) {
        this.isChanged = isChanged;
    }
}
