package bssolrus.com.quiz.model.ui;

import java.io.File;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class PhotoQuestion extends Question<String> {
    protected PhotoQuestion(String questionId, String text) {
        this.setQuestionId(questionId);
        this.setText(text);
        this.setQuestionType(QuestionTypes.QUESTION_TYPE_PHOTO);
    }

    @Override
    public boolean isAnswerSet() {
        return answer != null && new File(answer).exists();
    }
}
