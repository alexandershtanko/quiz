package bssolrus.com.quiz.model.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orm.SugarRecord;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/9/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiCategory extends SugarRecord<ApiCategory> {
    public String name;
    public String objectId;
}
