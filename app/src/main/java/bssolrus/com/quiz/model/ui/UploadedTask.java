package bssolrus.com.quiz.model.ui;

import com.orm.SugarRecord;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/10/15.
 */
public class UploadedTask extends SugarRecord<UploadedTask> {
    public String categoryId;
    public String marketId;
    public long timestamp;
}
