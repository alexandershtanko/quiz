package bssolrus.com.quiz.model.api;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class ApiLoginData {
    public String username;
    public String password;
}
