package bssolrus.com.quiz.model.ui;

import java.util.List;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class QuestionFactory {
    public static YesNoQuestion createYesNoQuestion(String questionId, String text) {
        return new YesNoQuestion(questionId, text);
    }

    public static VariantsQuestion createVariantsQuestion(String questionId,String text,List<String>variants)
    {
        return new VariantsQuestion(questionId,text,variants);
    }

    public static TextQuestion createTextQuestion(String questionId, String text)
    {
        return new TextQuestion(questionId, text);
    }

    public static PhotoQuestion createPhotoQuestion(String questionId,String text)
    {
        return new PhotoQuestion(questionId,text);
    }
}
