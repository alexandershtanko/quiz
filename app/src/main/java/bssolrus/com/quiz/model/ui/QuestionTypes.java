package bssolrus.com.quiz.model.ui;

/**
 * Created by aleksandr on 08.11.15.
 */
public class QuestionTypes {
    public final static int QUESTION_TYPE_YES_NO = 0;
    public final static int QUESTION_TYPE_VARIANTS = 1;
    public final static int QUESTION_TYPE_TEXT = 2;
    public final static int QUESTION_TYPE_PHOTO = 3;
}
