package bssolrus.com.quiz.model.api;

import java.util.List;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/11/15.
 */
public class ApiQuiz {
    public List<ApiAnswer> answers;
    public String categoryId;
    public String marketId;
}
