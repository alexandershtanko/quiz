package bssolrus.com.quiz.model.ui;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class YesNoQuestion extends Question<Integer> {
    public final static int ANSWER_NOT_SET = -1;
    public final static int ANSWER_NO = 0;
    public final static int ANSWER_YES = 1;


    protected YesNoQuestion(String questionId, String text) {
        this.setQuestionId(questionId);
        this.setText(text);
        this.setQuestionType(QuestionTypes.QUESTION_TYPE_YES_NO);
        this.setAnswer(ANSWER_NOT_SET);
    }

    @Override
    public boolean isAnswerSet() {
        return answer != ANSWER_NOT_SET;
    }
}
