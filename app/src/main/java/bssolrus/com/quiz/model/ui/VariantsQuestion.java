package bssolrus.com.quiz.model.ui;

import java.util.List;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/5/15.
 */
public class VariantsQuestion extends Question<Integer> {
    public static final int ANSWER_NOT_SET = -1;
    private List<String> variants;

    protected VariantsQuestion(String questionId, String text, List<String> variants) {
        this.setQuestionId(questionId);
        this.setText(text);
        this.setVariants(variants);
        this.setQuestionType(QuestionTypes.QUESTION_TYPE_VARIANTS);
        this.setAnswer(ANSWER_NOT_SET);
    }

    public List<String> getVariants() {
        return variants;
    }

    public void setVariants(List<String> variants) {
        this.variants = variants;
    }

    @Override
    public boolean isAnswerSet() {
        return answer!=ANSWER_NOT_SET;
    }
}
