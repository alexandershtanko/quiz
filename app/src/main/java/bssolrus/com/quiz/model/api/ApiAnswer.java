package bssolrus.com.quiz.model.api;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orm.SugarRecord;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE)
@JsonIgnoreProperties(ignoreUnknown = true)

public class ApiAnswer extends SugarRecord<ApiAnswer> {
    public String questionId;
    public String answer;
    public String marketId;
    public String categoryId;

}
