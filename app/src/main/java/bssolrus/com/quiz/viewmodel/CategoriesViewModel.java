package bssolrus.com.quiz.viewmodel;

import java.util.List;

import bssolrus.com.quiz.model.api.ApiCategory;
import bssolrus.com.quiz.model.api.ApiMarket;
import bssolrus.com.quiz.model.api.ApiQuestion;
import bssolrus.com.quiz.net.QuizServer;
import bssolrus.com.quiz.orm.OrmHelper;
import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public class CategoriesViewModel extends ViewModel {
    BehaviorSubject<List<ApiCategory>> categories = BehaviorSubject.create();
    BehaviorSubject<Throwable> categoryUpdateError = BehaviorSubject.create();

    BehaviorSubject<List<ApiMarket>> markets = BehaviorSubject.create();
    BehaviorSubject<Throwable> marketUpdateError = BehaviorSubject.create();

    BehaviorSubject<List<ApiQuestion>> questions = BehaviorSubject.create();
    BehaviorSubject<Throwable> questionUpdateError = BehaviorSubject.create();

    ApiMarket market = null;

    @Override
    public void subscribe(CompositeSubscription s) {

        s.add(QuizServer.getService().getCategories()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())
                .subscribe(resp -> {
                    if (resp.error == null) {
                        resp.results.remove(null);
                        OrmHelper.saveCategories(resp.results);
                        categories.onNext(resp.results);
                    } else categoryUpdateError.onNext(new Throwable(resp.error));
                }, categoryUpdateError::onNext));

        s.add(categoryUpdateError.asObservable().subscribeOn(Schedulers.io())
                .map(throwable -> OrmHelper.getCategories())
                .subscribe(apiCategories -> {
                    if (apiCategories != null) {
                        categories.onNext(apiCategories);
                    }
                }));

        s.add(QuizServer.getService().getMarkets()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())

                .subscribe(resp -> {
                    if (resp.error == null) {
                        resp.results.remove(null);
                        OrmHelper.saveMarkets(resp.results);
                        markets.onNext(resp.results);
                    } else marketUpdateError.onNext(new Throwable(resp.error));
                }, marketUpdateError::onNext));

        s.add(marketUpdateError.asObservable().subscribeOn(Schedulers.io())
                .map(throwable -> OrmHelper.getMarkets())
                .subscribe(apiMarkets -> {
                    if (apiMarkets != null)
                        markets.onNext(apiMarkets);
                }));


        s.add(QuizServer.getService().getQuestions()
                .subscribeOn(Schedulers.io())
                .unsubscribeOn(Schedulers.io())

                .subscribe(resp -> {
                    if (resp.error == null) {
                        OrmHelper.saveQuestions(resp.results);
                        questions.onNext(resp.results);
                    } else questionUpdateError.onNext(new Throwable(resp.error));
                }, questionUpdateError::onNext));

    }

    public Observable<List<ApiCategory>> getCategories() {
        return categories.asObservable();
    }

    public Observable<Throwable> getCategoryUpdateError() {
        return categoryUpdateError.asObservable();
    }

    public Observable<List<ApiMarket>> getMarkets() {
        return markets.asObservable();
    }

    public Observable<Throwable> getMarketUpdateError() {
        return marketUpdateError.asObservable();
    }

    public Observable<Throwable> getQuestionUpdateError() {
        return marketUpdateError.asObservable();
    }

    public void selectMarket(ApiMarket market) {
        if (market != null)
            this.market=market;
    }



    public ApiMarket getMarket(Integer pos) {
        if (markets.getValue() == null || markets.getValue().size() <= pos)
            return null;
        return markets.getValue().get(pos);
    }

    public ApiMarket getSelectedMarket() {
        return market;
    }
}
