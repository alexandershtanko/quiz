package bssolrus.com.quiz.viewmodel;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public abstract class ViewModel {
    CompositeSubscription compositeSubscription;

    public void subscribeToDataStore() {
        unsubscribeFromDataStore();
        compositeSubscription = new CompositeSubscription();
        subscribe(compositeSubscription);

    }

    protected abstract void subscribe(CompositeSubscription compositeSubscription);

    public void unsubscribeFromDataStore() {
        if (compositeSubscription != null) {
            compositeSubscription.clear();
            compositeSubscription = null;
        }
    }

}
