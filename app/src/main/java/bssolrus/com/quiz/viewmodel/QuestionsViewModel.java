package bssolrus.com.quiz.viewmodel;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import bssolrus.com.quiz.Settings;
import bssolrus.com.quiz.model.api.ApiAnswer;
import bssolrus.com.quiz.model.api.ApiBatchRequest;
import bssolrus.com.quiz.model.api.ApiBatchResponse;
import bssolrus.com.quiz.model.api.ApiRequest;
import bssolrus.com.quiz.net.QuizServer;
import bssolrus.com.quiz.orm.OrmHelper;
import rx.Observable;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public class QuestionsViewModel extends ViewModel {


    @Override
    public void subscribe(CompositeSubscription s) {
    }

    public void saveAnswers(Context context, List<ApiAnswer> answers, String categoryId, String marketId) {
        Settings.saveLastQuizCategoryId(context, categoryId);
        Settings.saveLastQuizMarketId(context, marketId);
        OrmHelper.saveAnswers(answers);
    }

    public Observable<List<ApiBatchResponse>> sendAnswers(Context context, List<ApiAnswer> answers, String categoryId, String marketId)
    {
        ApiBatchRequest batchRequest=new ApiBatchRequest();
        batchRequest.requests=new ArrayList<>();
        for(ApiAnswer answer:answers)
        {
            ApiRequest<ApiAnswer> request=new ApiRequest<>();
            request.body=answer;
            request.path="/1/classes/answer";
            batchRequest.requests.add(request);
        }

        return QuizServer.getService().sendBatchRequest(batchRequest);
    }
}
