package bssolrus.com.quiz.view;

import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxAdapterView;

import java.util.ArrayList;
import java.util.List;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.model.api.ApiCategory;
import bssolrus.com.quiz.model.api.ApiMarket;
import bssolrus.com.quiz.ui.activity.MainActivity;
import bssolrus.com.quiz.ui.adapter.CategoriesAdapter;
import bssolrus.com.quiz.viewmodel.CategoriesViewModel;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public class CategoriesView extends LinearLayout {
    @Bind(R.id.markets)
    AppCompatSpinner marketsSpinner;
    @Bind(R.id.categories)
    RecyclerView categoriesView;

    CategoriesAdapter categoriesAdapter;
    SpinnerAdapter marketsAdapter;

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        categoriesView.setLayoutManager(gridLayoutManager);
        categoriesAdapter = new CategoriesAdapter();
        categoriesView.setAdapter(categoriesAdapter);
    }

    public CategoriesView(Context context) {
        super(context);
    }

    public CategoriesView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static class ViewBinder extends AbstractViewBinder {
        private final CategoriesView view;
        private final CategoriesViewModel viewModel;
        private final MainActivity activity;

        public ViewBinder(MainActivity activity, CategoriesView view, CategoriesViewModel viewModel) {
            this.view = view;
            this.viewModel = viewModel;
            this.activity=activity;
        }

        @Override
        protected void bindInternal(CompositeSubscription s) {
            s.add(viewModel.getMarkets()
                    .map(apiMarkets -> {
                        List<String> marketNames = new ArrayList<>();
                        for (ApiMarket market : apiMarkets) {
                            marketNames.add(market.name);
                        }
                        return marketNames;
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(markets -> {
                                view.setMarkets(markets);
                                s.add(viewModel.getCategories()
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(categories -> {
                                            view.categoriesAdapter.setItems(categories);
                                            ApiMarket market = viewModel.getMarket(0);
                                            if (market != null)
                                                view.categoriesAdapter.setMarketId(market.objectId);
                                        }));
                            }
                    ));

            s.add(viewModel.getCategoryUpdateError()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(throwable -> {
                        Toast.makeText(view.getContext(), R.string.error_network_categories, Toast.LENGTH_LONG).show();

                    }));

            s.add(viewModel.getMarketUpdateError()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(throwable -> {
                        Toast.makeText(view.getContext(), R.string.error_network_markets, Toast.LENGTH_LONG).show();
                    }));

            s.add(viewModel.getQuestionUpdateError()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(throwable -> {
                        Toast.makeText(view.getContext(), R.string.error_network_questions, Toast.LENGTH_LONG).show();
                    }));

            s.add(rx.Observable.create(subscriber ->
                    view.categoriesAdapter.setOnItemClickListener((parent, view1, position, id) ->
                            subscriber.onNext(view.categoriesAdapter.getItem(position))))
                    .subscribe(category -> activity.selectQuestionsFragment(((ApiCategory)category).objectId,viewModel.getSelectedMarket().objectId)));

            s.add(RxAdapterView.itemSelections(view.marketsSpinner)
                    .filter(pos -> pos >= 0)
                    .map(viewModel::getMarket)
                    .filter(market -> market != null)
                    .subscribe(market -> {
                        viewModel.selectMarket(market);
                        view.categoriesAdapter.setMarketId(market.objectId);
                    }));


        }
    }

    private void setMarkets(List<String> marketsNames) {
        marketsAdapter = new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, marketsNames);
        marketsSpinner.setAdapter(marketsAdapter);
    }
}
