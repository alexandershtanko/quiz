package bssolrus.com.quiz.view;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public abstract class AbstractViewBinder {
    CompositeSubscription compositeSubscription;

    public void bind() {
        unbind();
        compositeSubscription = new CompositeSubscription();
        bindInternal(compositeSubscription);
    }


    protected abstract void bindInternal(CompositeSubscription compositeSubscription);

    public void unbind() {
        if (compositeSubscription != null) {
            compositeSubscription.clear();
            compositeSubscription = null;
        }
    }
}
