package bssolrus.com.quiz.view;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.model.api.ApiQuestion;
import bssolrus.com.quiz.orm.ApiModelConverter;
import bssolrus.com.quiz.orm.OrmHelper;
import bssolrus.com.quiz.ui.activity.MainActivity;
import bssolrus.com.quiz.ui.adapter.QuestionsListAdapter;
import bssolrus.com.quiz.util.PhotoProxy;
import bssolrus.com.quiz.viewmodel.QuestionsViewModel;
import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by aleksandr on 20.12.15.
 */
public class QuestionsView extends CoordinatorLayout {

    @Bind(R.id.categories)
    RecyclerView rvCategories;
    @Bind(R.id.fabSend)
    FloatingActionButton bSend;

    QuestionsListAdapter adapter = new QuestionsListAdapter();

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        rvCategories.setLayoutManager(new LinearLayoutManager(getContext()));
        rvCategories.setAdapter(adapter);
    }

    public QuestionsView(Context context) {
        super(context);
    }

    public QuestionsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static class ViewBinder extends AbstractViewBinder {
        private final QuestionsView view;
        private final QuestionsViewModel viewModel;
        private final String categoryId;
        private final String marketId;
        private final MainActivity activity;
        int a=0;

        public ViewBinder(MainActivity activity, QuestionsView view, QuestionsViewModel viewModel, PhotoProxy photoProxy, String categoryId, String marketId) {
            this.view = view;
            this.viewModel = viewModel;
            this.categoryId = categoryId;
            this.marketId = marketId;
            this.activity=activity;

            view.adapter.setPhotoProxy(photoProxy);
        }

        @Override
        protected void bindInternal(CompositeSubscription s) {

            s.add(Observable.create(new Observable.OnSubscribe<List<ApiQuestion>>() {
                @Override
                public void call(Subscriber<? super List<ApiQuestion>> subscriber) {
                    subscriber.onNext(OrmHelper.getQuestions(categoryId));
                }
            })
                    .map(apiQuestions -> ApiModelConverter.convertApiModelQuestionsToQuestions(apiQuestions, OrmHelper.getAnswers()))
                    .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(view.adapter::updateQuestions));



            s.add(Observable.create(subscriber ->
                    view.bSend.setOnClickListener(v -> {
                        subscriber.onNext("ok");
                    }))
                    .map(aVoid -> ApiModelConverter.getApiModelAnswersForSend(view.adapter.getQuestions(), categoryId, marketId))
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .flatMap(answers -> viewModel.sendAnswers(view.getContext(), answers, categoryId, marketId).subscribeOn(Schedulers.newThread())
                    )
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(response -> {
                        OrmHelper.clearAnswers();
                        Toast.makeText(view.getContext(), "Ответы успешно отправлены", Toast.LENGTH_LONG).show();
                        activity.selectCategoriesFragment();

                    }, throwable -> {
                        Log.e("Network error", "Error", throwable);

                        Toast.makeText(view.getContext(), "При отправке ответов произошла ошибка, попробуйте еще раз", Toast.LENGTH_LONG).show();
                    }));


        }
    }


}
