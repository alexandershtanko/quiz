package bssolrus.com.quiz.orm;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import bssolrus.com.quiz.model.api.ApiAnswer;
import bssolrus.com.quiz.model.api.ApiQuestion;
import bssolrus.com.quiz.model.api.ApiQuestionTypes;
import bssolrus.com.quiz.model.ui.Question;
import bssolrus.com.quiz.model.ui.QuestionFactory;
import bssolrus.com.quiz.model.ui.QuestionTypes;

/**
 * Created by aleksandr on 08.11.15.
 */
public class ApiModelConverter {
    private static final String TAG = "ApiModelConverter";

    public static List<Question> convertApiModelQuestionsToQuestions(List<ApiQuestion> apiQuestions, List<ApiAnswer> apiAnswers) {
        List<ApiAnswer> apiAnswers1 = new ArrayList<>();
        if (apiAnswers != null) {
            apiAnswers1.addAll(apiAnswers);
        }

        List<Question> questions = new ArrayList<>();

        for (ApiQuestion apiQuestion : apiQuestions) {
            Question question = createQuestion(apiQuestion);

            if (question != null && apiAnswers != null) {
                Iterator<ApiAnswer> iterator = apiAnswers1.iterator();
                while (iterator.hasNext()) {
                    ApiAnswer apiAnswer = iterator.next();
                    if (apiAnswer.questionId == apiQuestion.objectId) {
                        question.setAnswer(getAnswer(apiAnswer.answer, apiQuestion.questionType));
                        iterator.remove();
                    }
                }
            }

            if (question != null) {
                questions.add(question);
            }
        }

        return questions;
    }

    private static java.io.Serializable getAnswer(String answer, String questionType) {
        try {
            switch (questionType) {
                case ApiQuestionTypes.YES_NO_QUESTION:
                    return Integer.parseInt(answer);

                case ApiQuestionTypes.VARIANTS_QUESTION:
                    return Integer.parseInt(answer);

                case ApiQuestionTypes.TEXT_QUESTION:
                    return answer;

                case ApiQuestionTypes.PHOTO_QUESTION:
                    return answer;

            }
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

    private static Question createQuestion(ApiQuestion apiQuestion) {
        Question question = null;
        switch (apiQuestion.questionType) {
            case ApiQuestionTypes.YES_NO_QUESTION:
                question = (QuestionFactory.createYesNoQuestion(apiQuestion.objectId, apiQuestion.text));
                break;
            case ApiQuestionTypes.VARIANTS_QUESTION:
                List<String> variants = null;
                if (apiQuestion.variants != null)
                    variants = new ArrayList<>(Arrays.asList(apiQuestion.variants.trim().split(",")));
                question = (QuestionFactory.createVariantsQuestion(apiQuestion.objectId, apiQuestion.text, variants));
                break;
            case ApiQuestionTypes.TEXT_QUESTION:
                question = (QuestionFactory.createTextQuestion(apiQuestion.objectId, apiQuestion.text));
                break;
            case ApiQuestionTypes.PHOTO_QUESTION:
                question = (QuestionFactory.createPhotoQuestion(apiQuestion.objectId, apiQuestion.text));
                break;
        }
        return question;
    }


    public static List<ApiAnswer> getApiModelAnswersForSaving(List<Question> questions, String categoryId, String marketId) {
        List<ApiAnswer> apiAnswers = new ArrayList<ApiAnswer>();
        for (Question question : questions) {

            if (question.getAnswer() != null) {
                ApiAnswer apiAnswer = getApiModelAnswer(question, categoryId, marketId);


                apiAnswers.add(apiAnswer);
            }
        }
        return apiAnswers;
    }

    public static List<ApiAnswer> getApiModelAnswersForSend(List<Question> questions, String categoryId, String marketId) {
        List<ApiAnswer> apiAnswers = new ArrayList<ApiAnswer>();
        for (Question question : questions) {

            if (question.getAnswer() != null) {
                ApiAnswer apiAnswer = getApiModelAnswer(question, categoryId, marketId);

                if (question.getQuestionType() == QuestionTypes.QUESTION_TYPE_PHOTO) {
                    String path = apiAnswer.answer;
                    apiAnswer.answer = getBase64(path);
                }

                apiAnswers.add(apiAnswer);
            }
        }
        return apiAnswers;
    }

    private static String getBase64(String path) {
        try {
            if (path != null) {
                Bitmap b = BitmapFactory.decodeFile(path);
                Bitmap bm;
                if (b.getWidth() > b.getHeight())
                    bm = Bitmap.createScaledBitmap(b, 800, b.getHeight() * 800 / b.getWidth(), false);
                else
                    bm = Bitmap.createScaledBitmap(b, b.getWidth() * 800 / b.getHeight(), 800, false);


                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 70, baos); //bm is the bitmap object

                return Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);

            }
        } catch (Exception e) {
        }
        return "";
    }

    @NonNull
    public static ApiAnswer getApiModelAnswer(Question question, String categoryId, String marketId) {
        ApiAnswer apiAnswer = new ApiAnswer();
        apiAnswer.answer = question.getAnswer().toString();
        apiAnswer.questionId = question.getQuestionId();
        apiAnswer.categoryId = categoryId;
        apiAnswer.marketId = marketId;
        return apiAnswer;
    }


}
