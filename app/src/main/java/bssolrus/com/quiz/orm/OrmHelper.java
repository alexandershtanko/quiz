package bssolrus.com.quiz.orm;

import android.util.Log;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import bssolrus.com.quiz.model.ui.UploadedTask;
import bssolrus.com.quiz.model.api.ApiAnswer;
import bssolrus.com.quiz.model.api.ApiCategory;
import bssolrus.com.quiz.model.api.ApiMarket;
import bssolrus.com.quiz.model.api.ApiQuestion;


/**
 * Created by aleksandr on 06.11.15.
 */
public class OrmHelper {

    private static final String TAG = "OrmHelper";


    public static List<ApiQuestion> getQuestions(String categoryId) {
        try {
            return ApiQuestion.find(ApiQuestion.class, "category_id='" + categoryId+"'");
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    public static void saveQuestions(List<ApiQuestion> questions) {
        clearQuestions();
        for (ApiQuestion question : questions) {
            question.save();
        }
    }

    public static void clearQuestions() {
        try {
            ApiQuestion.deleteAll(ApiQuestion.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }


    public static void saveAnswers(List<ApiAnswer> answers) {
        clearAnswers();
        for (ApiAnswer answer : answers) {
            answer.save();
        }
    }

    public static List<ApiAnswer> getAnswers() {
        try {
            return ApiAnswer.find(ApiAnswer.class, null);
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return Collections.EMPTY_LIST;
        }

    }

    public static void clearAnswers() {
        try {
            ApiAnswer.deleteAll(ApiAnswer.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public static boolean hasSavedAnswers() {
        try {
            return ApiAnswer.count(ApiAnswer.class, null, null) > 0;
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return false;
        }
    }

    public static ApiAnswer getFirstSavedAnswer() {
        try {
            List<ApiAnswer> answers = ApiAnswer.find(ApiAnswer.class, null, null, null, null, "1");
            if (answers != null && answers.size() > 0)
                answers.get(0);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

    public static ApiQuestion getQuestion(String questionId) {
        try {
            List<ApiQuestion> questions = ApiAnswer.find(ApiQuestion.class, "question_id='" + questionId+"'");
            if (questions != null && questions.size() > 0)
                return questions.get(0);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

    public static ApiCategory getCategory(String categoryId) {
        try {
            List<ApiCategory> categories = ApiAnswer.find(ApiCategory.class, "category_id='" + categoryId+"'");
            if (categories != null && categories.size() > 0)
                return categories.get(0);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }

    public static ApiMarket getMarket(String marketId) {
        try {
            List<ApiMarket> markets = ApiAnswer.find(ApiMarket.class, "market_id='" + marketId+"'");
            if (markets != null && markets.size() > 0)
                return markets.get(0);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
        return null;
    }


    public static void saveCategories(List<ApiCategory> categories) {
        clearCategories();
        for (ApiCategory category : categories) {
            category.save();
        }
    }

    public static void clearCategories() {
        try {
            ApiCategory.deleteAll(ApiCategory.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public static List<ApiCategory> getCategories() {
        try {
            return ApiCategory.find(ApiCategory.class, null);
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return Collections.EMPTY_LIST;
        }
    }


    public static void saveMarkets(List<ApiMarket> markets) {
        clearMarkets();
        for (ApiMarket market : markets) {
            market.save();
        }
    }

    public static void clearMarkets() {
        try {
            ApiMarket.deleteAll(ApiMarket.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
        }
    }

    public static List<ApiMarket> getMarkets() {
        try {
            return ApiMarket.listAll(ApiMarket.class);
        } catch (Exception e) {
            Log.e(TAG, "", e);
            return Collections.EMPTY_LIST;
        }
    }

    public static void saveToLog(String categoryId, String marketId) {
        UploadedTask task = new UploadedTask();
        task.categoryId = categoryId;
        task.marketId = marketId;
        task.timestamp = new Date().getTime();

        task.save();
    }

    public static long getFromLog(String categoryId, String marketId) {
        try {
            List<UploadedTask> tasks = UploadedTask.find(UploadedTask.class, "category_id='" + categoryId + "' and market_id='" + marketId +"'", null, null, "timestamp desc", "1");
            if (tasks != null && tasks.size() > 0)
                return tasks.get(0).timestamp;
        } catch (Exception e) {
        }
        return -1;
    }
}
