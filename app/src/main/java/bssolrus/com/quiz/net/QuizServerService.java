package bssolrus.com.quiz.net;


import java.util.List;

import bssolrus.com.quiz.model.api.ApiBatchRequest;
import bssolrus.com.quiz.model.api.ApiBatchResponse;
import bssolrus.com.quiz.model.api.ApiCategory;
import bssolrus.com.quiz.model.api.ApiLoginData;
import bssolrus.com.quiz.model.api.ApiMarket;
import bssolrus.com.quiz.model.api.ApiModelToken;
import bssolrus.com.quiz.model.api.ApiQuestion;
import bssolrus.com.quiz.model.api.ApiResponse;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import rx.Observable;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */


public interface QuizServerService {
    @GET("classes/question")
    Observable<ApiResponse<ApiQuestion>> getQuestions();

    @GET("classes/category")
    Observable<ApiResponse<ApiCategory>> getCategories();

    @GET("classes/market")
    Observable<ApiResponse<ApiMarket>> getMarkets();

    @POST("batch")
    Observable<List<ApiBatchResponse>> sendBatchRequest(@Body ApiBatchRequest request);

    @GET("login")
    Observable<ApiModelToken> login(@Body ApiLoginData apiLoginData);
}
