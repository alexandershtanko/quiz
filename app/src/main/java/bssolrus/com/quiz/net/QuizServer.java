package bssolrus.com.quiz.net;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import retrofit.JacksonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class QuizServer {
    private static final String QUIZ_SERVER_URL = "https://api.parse.com/1/";
    public static final String APPLICATION_ID = "xAAC5HCaNR3o0HSMLVFqVRJihs5Kw09PHPOOCqst";
    public static final String REST_API_KEY = "Ss4C1EULUWI9ikLGfwSVwXKDD44o0XlxQO3pG4DX";

    private static QuizServer instance;
    private final QuizServerService quizServerService;


    private QuizServer() {
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("X-Parse-Application-Id", APPLICATION_ID)
                        .header("X-Parse-REST-API-Key", REST_API_KEY)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(QUIZ_SERVER_URL)
                .addConverterFactory(JacksonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient)
                .build();
        quizServerService = retrofit.create(QuizServerService.class);
    }

    public static synchronized QuizServerService getService() {
        if (instance == null)
            instance = new QuizServer();
        return instance.quizServerService;
    }


}
