package bssolrus.com.quiz.ui.fragment;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.ui.activity.MainActivity;
import bssolrus.com.quiz.util.PhotoProxy;
import bssolrus.com.quiz.view.QuestionsView;
import bssolrus.com.quiz.viewmodel.QuestionsViewModel;


public class QuestionsFragment extends Fragment {

    public static final String CATEGORY_ID = "category_id";
    public static final String MARKET_ID = "market_id";
    private static final int REQUEST_IMAGE_CAPTURE = 1000;


    private MainActivity activity;
    private QuestionsViewModel viewModel = new QuestionsViewModel();
    private QuestionsView.ViewBinder viewBinder;
    private PhotoProxy photoProxy = new PhotoProxy();


    public QuestionsFragment() {
    }

    public static QuestionsFragment newInstance(String categoryId, String marketId) {
        QuestionsFragment fragment = new QuestionsFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_ID, categoryId);
        args.putString(MARKET_ID, marketId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_questions, container, false);
        QuestionsView questionsView = (QuestionsView) view;


        String categoryId = getArguments().getString(CATEGORY_ID);
        String marketId = getArguments().getString(MARKET_ID);

        photoProxy.setOnRequestPhotoListener(outputFile -> {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(outputFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        });

        viewBinder = new QuestionsView.ViewBinder(activity,questionsView, viewModel, photoProxy, categoryId, marketId);

        updateToolbar();

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.subscribeToDataStore();
        viewBinder.bind();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewBinder.unbind();
        viewModel.unsubscribeFromDataStore();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel = null;
    }


    private void updateToolbar() {
        activity.getToolbar().setTitle(R.string.app_name);
        activity.getToolbar().setSubtitle(null);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            photoProxy.sendSuccess();
        }
    }


}
