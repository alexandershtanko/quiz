package bssolrus.com.quiz.ui.listener;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/10/15.
 */
public interface OnSuccessListener {
    void onSuccess();
}
