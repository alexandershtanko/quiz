package bssolrus.com.quiz.ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.ui.activity.MainActivity;
import bssolrus.com.quiz.view.CategoriesView;
import bssolrus.com.quiz.viewmodel.CategoriesViewModel;


public class CategoriesFragment extends Fragment {

    private MainActivity activity;
    private CategoriesViewModel viewModel = new CategoriesViewModel();
    private CategoriesView.ViewBinder viewBinder;

    public CategoriesFragment() {
    }

    public static CategoriesFragment newInstance() {
        CategoriesFragment fragment = new CategoriesFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categories, container, false);
        CategoriesView categoriesView = (CategoriesView) view;
        viewBinder = new CategoriesView.ViewBinder(activity,categoriesView, viewModel);


        updateToolbar();

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.subscribeToDataStore();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewBinder.bind();
    }

    @Override
    public void onPause() {
        super.onPause();
        viewBinder.unbind();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewModel.unsubscribeFromDataStore();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel = null;
    }


    private void updateToolbar() {
        activity.getToolbar().setTitle(R.string.app_name);
        activity.getToolbar().setSubtitle(null);
    }


}
