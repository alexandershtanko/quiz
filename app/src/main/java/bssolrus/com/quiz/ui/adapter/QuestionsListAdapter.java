package bssolrus.com.quiz.ui.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.model.ui.PhotoQuestion;
import bssolrus.com.quiz.model.ui.Question;
import bssolrus.com.quiz.model.ui.QuestionTypes;
import bssolrus.com.quiz.model.ui.TextQuestion;
import bssolrus.com.quiz.model.ui.VariantsQuestion;
import bssolrus.com.quiz.model.ui.YesNoQuestion;
import bssolrus.com.quiz.util.PhotoProxy;

/**
 * Created by aleksandr on 04.11.15.
 */
public class QuestionsListAdapter extends RecyclerView.Adapter {
    private  PhotoProxy photoProxy;
    private List<Question> questions = new ArrayList<>();
    private volatile boolean onBind = false;

    public QuestionsListAdapter() {

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View view;

        switch (viewType) {
            case QuestionTypes.QUESTION_TYPE_YES_NO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_yes_no, parent, false);
                viewHolder = new YesNoQuestionViewHolder(view);
                break;
            case QuestionTypes.QUESTION_TYPE_VARIANTS:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_variants, parent, false);
                viewHolder = new VariantsQuestionViewHolder(view);
                break;
            case QuestionTypes.QUESTION_TYPE_TEXT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_text, parent, false);
                viewHolder = new TextQuestionViewHolder(view);
                break;
            case QuestionTypes.QUESTION_TYPE_PHOTO:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_question_photo, parent, false);
                viewHolder = new PhotoQuestionViewHolder(view);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBind = true;
        ((QuestionViewHolder) holder).populate(position);
        onBind = false;
    }

    @Override
    public int getItemViewType(int position) {
        return questions.get(position).getQuestionType();
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }

    public void updateQuestions(List<Question> questions) {
        this.questions = questions;
        if (!onBind)
            notifyDataSetChanged();
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setPhotoProxy(PhotoProxy photoProxy) {
        this.photoProxy = photoProxy;
    }


    public abstract class QuestionViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvNumber;
        private final TextView tvText;

        public QuestionViewHolder(View itemView) {
            super(itemView);
            tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
            tvText = (TextView) itemView.findViewById(R.id.tvText);
        }

        public void populate(int position) {
            Question question = questions.get(position);
            Context context = itemView.getContext();
            tvText.setText(question.getText());
            String pos = position + 1 + ".";
            tvNumber.setText(pos);

            if (question.isAnswerSet()) {
                //itemView.setBackgroundColor(context.getResources().getColor(R.color.colorQuestionAnswered));

            } else {
                //itemView.setBackgroundColor(context.getResources().getColor(R.color.colorQuestionNotAnswered));
            }
        }
    }

    public class YesNoQuestionViewHolder extends QuestionViewHolder {

        private final Button bYes;
        private final Button bNo;

        public YesNoQuestionViewHolder(View itemView) {
            super(itemView);


            bYes = (Button) itemView.findViewById(R.id.bYes);
            bNo = (Button) itemView.findViewById(R.id.bNo);

            bYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    YesNoQuestion yesNoQuestion = (YesNoQuestion) questions.get(position);
                    yesNoQuestion.setAnswer(YesNoQuestion.ANSWER_YES);
                    if (!onBind)
                        notifyItemChanged(position);
                }
            });

            bNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    YesNoQuestion yesNoQuestion = (YesNoQuestion) questions.get(position);
                    yesNoQuestion.setAnswer(YesNoQuestion.ANSWER_NO);
                    if (!onBind)
                        notifyItemChanged(position);
                }
            });


        }


        @Override
        public void populate(int position) {
            super.populate(position);
            Context context = itemView.getContext();
            YesNoQuestion yesNoQuestion = (YesNoQuestion) questions.get(position);
            int answer = yesNoQuestion.getAnswer();
            switch (answer) {
                case YesNoQuestion.ANSWER_NO:
                    bNo.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    bYes.setTextColor(ContextCompat.getColor(context, android.R.color.primary_text_light));
                    break;

                case YesNoQuestion.ANSWER_YES:
                    bNo.setTextColor(ContextCompat.getColor(context, android.R.color.primary_text_light));
                    bYes.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                    break;

                case YesNoQuestion.ANSWER_NOT_SET:
                    bNo.setTextColor(ContextCompat.getColor(context, android.R.color.primary_text_light));
                    bYes.setTextColor(ContextCompat.getColor(context, android.R.color.primary_text_light));
                    break;
            }

        }
    }

    public class VariantsQuestionViewHolder extends QuestionViewHolder {


        private final RadioGroup rgVariants;

        public VariantsQuestionViewHolder(View itemView) {
            super(itemView);
            rgVariants = (RadioGroup) itemView.findViewById(R.id.rgVariants);
            rgVariants.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int position = getAdapterPosition();
                    VariantsQuestion variantsQuestion = (VariantsQuestion) questions.get(position);
                    View radioButton = group.findViewById(checkedId);
                    int index = group.indexOfChild(radioButton);
                    variantsQuestion.setAnswer(index);
                    if (!onBind)
                        notifyItemChanged(position);
                }
            });


        }

        @Override
        public void populate(int position) {
            super.populate(position);
            Context context = itemView.getContext();
            VariantsQuestion variantsQuestion = (VariantsQuestion) questions.get(position);
            int answer = variantsQuestion.getAnswer();
            rgVariants.removeAllViews();

            int i = 0;
            if (variantsQuestion.getVariants() == null)
                return;

            for (String variant : variantsQuestion.getVariants()) {
                RadioButton radioButton = new RadioButton(context);
                radioButton.setText(variant);
                radioButton.setId(i);
                rgVariants.addView(radioButton);
                i++;
            }
            rgVariants.check(answer);

        }
    }


    public class TextQuestionViewHolder extends QuestionViewHolder {
        private final EditText etAnswer;

        public TextQuestionViewHolder(View itemView) {
            super(itemView);

            etAnswer = (EditText) itemView.findViewById(R.id.etAnswer);
            etAnswer.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    int position = getAdapterPosition();
                    String answer = etAnswer.getText().toString();
                    TextQuestion textQuestion = (TextQuestion) questions.get(position);
                    textQuestion.setAnswer(answer);

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

        @Override
        public void populate(int position) {
            super.populate(position);
            TextQuestion textQuestion = (TextQuestion) questions.get(position);
            etAnswer.setText(textQuestion.getAnswer());
        }
    }

    public class PhotoQuestionViewHolder extends QuestionViewHolder {
        private final Button bReplace, bRemove;
        private final ImageView ivPhoto;

        public PhotoQuestionViewHolder(final View itemView) {
            super(itemView);
            bReplace = (Button) itemView.findViewById(R.id.bTakePhoto);
            bRemove = (Button) itemView.findViewById(R.id.bRemove);
            ivPhoto = (ImageView) itemView.findViewById(R.id.ivPhoto);

            bRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    PhotoQuestion photoQuestion = (PhotoQuestion) questions.get(position);
                    photoQuestion.setAnswer(null);
                    if (!onBind)
                        notifyItemChanged(position);
                }
            });

            bReplace.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final int position = getAdapterPosition();
                    final PhotoQuestion photoQuestion = (PhotoQuestion) questions.get(position);
                    final File photoFile = photoProxy.createImageFile(itemView.getContext());
                    if (photoFile != null) {
                        photoProxy.setOnTakePhotoListener(new PhotoProxy.OnTakePhotoListener() {
                            @Override
                            public void onTakePhoto() {
                                photoQuestion.setAnswer(photoFile.getAbsolutePath());
                                if (!onBind)
                                    notifyItemChanged(position);
                            }
                        });

                        photoProxy.requestPhoto(photoFile);
                    }
                }
            });
        }


        @Override
        public void populate(int position) {
            super.populate(position);
            PhotoQuestion photoQuestion = (PhotoQuestion) questions.get(position);
            Context context = itemView.getContext();
            Glide.with(context).load(photoQuestion.getAnswer()).into(ivPhoto);

            if (photoQuestion.isAnswerSet()) {
                bRemove.setVisibility(View.VISIBLE);
            } else {
                bRemove.setVisibility(View.INVISIBLE);
            }
        }


    }
}
