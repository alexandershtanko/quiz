package bssolrus.com.quiz.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bssolrus.com.quiz.R;
import bssolrus.com.quiz.model.api.ApiCategory;
import bssolrus.com.quiz.orm.OrmHelper;

/**
 * Created by aleksandr on 04.11.15.
 */
public class CategoriesAdapter extends RecyclerView.Adapter {

    private List<ApiCategory> categories = new ArrayList<>();
    private List<ApiCategory> savedCategories = new ArrayList<>();
    private Map<String, Long> taskMap = new HashMap<>();

    private volatile boolean onBind = false;
    private AdapterView.OnItemClickListener onItemClickListener;

    public CategoriesAdapter() {

    }

    public void setMarketId(String marketId) {
        taskMap.clear();
        categories.clear();
        notifyDataSetChanged();

        int i = 0;
        for (ApiCategory category : savedCategories) {

            long timestamp = OrmHelper.getFromLog(category.objectId, marketId);
            if (timestamp != -1) {
                taskMap.put(category.objectId, timestamp);
            }

            categories.add(category);
            notifyItemInserted(i);
            i++;
        }


    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        onBind = true;
        ((CategoryViewHolder) holder).populate(position);
        onBind = false;
    }


    @Override
    public int getItemCount() {
        return categories.size();
    }

    public void updateCategories(List<ApiCategory> categories) {
        this.categories = categories;
        savedCategories = new ArrayList<>(categories);
        if (!onBind)
            notifyDataSetChanged();
    }

    public void setOnItemClickListener(AdapterView.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public boolean isAlreadySentToday(int position) {
        ApiCategory category = categories.get(position);
        return taskMap.containsKey(category.objectId) && isSameDay(taskMap.get(category.objectId), new Date().getTime());
    }

    boolean isSameDay(long time1, long time2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(new Date(time1));
        cal2.setTime(new Date(time2));
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public void setItems(List<ApiCategory> apiCategories) {
        updateCategories(apiCategories);
    }

    public ApiCategory getItem(int position) {
        return categories.get(position);
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        private final TextView tvName;

        public CategoryViewHolder(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                int position = getAdapterPosition();
                if (onItemClickListener != null)
                    onItemClickListener.onItemClick(null, itemView, position, getItemId());
            });
            tvName = (TextView) itemView.findViewById(R.id.tvName);
        }


        public void populate(int position) {
            ApiCategory category = categories.get(position);
            tvName.setText(category.name);
            if (isAlreadySentToday(position)) {
                tvName.setCompoundDrawablesWithIntrinsicBounds(itemView.getContext().getResources().getDrawable(R.drawable.ic_done_black_24dp), null, null, null);
            } else
                tvName.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }
}
