package bssolrus.com.quiz.util;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by
 *
 * @author Alexander Shtanko
 *         on 11/6/15.
 */
public class PhotoProxy {
    private static final String TAG = "PhotoProxy";
    private OnTakePhotoListener onTakePhotoListener;
    private OnRequestPhotoListener onRequestPhotoListener;

    public void setOnTakePhotoListener(OnTakePhotoListener onTakePhotoListener)
    {
        this.onTakePhotoListener=onTakePhotoListener;
    }

    public void setOnRequestPhotoListener(OnRequestPhotoListener onRequestPhotoListener)
    {
        this.onRequestPhotoListener=onRequestPhotoListener;
    }

    public void requestPhoto(File outputFile) {
        if(onRequestPhotoListener!=null)
        {
            onRequestPhotoListener.onPhotoRequest(outputFile);
        }
    }

    public void sendSuccess() {
        if(onTakePhotoListener!=null)
            onTakePhotoListener.onTakePhoto();
    }

    public interface OnTakePhotoListener {
        void onTakePhoto();
    }
    public interface OnRequestPhotoListener {
        void onPhotoRequest(File outputFile);
    }

    public File createImageFile(Context context) {
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = context.getExternalCacheDir();
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            return image;
        }
        catch (Exception e)
        {
            Log.e(TAG,"",e);
        }
        return null;
    }
}
